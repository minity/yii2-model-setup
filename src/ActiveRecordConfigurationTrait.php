<?php
/*
 * This file is part of the minity/yii2-model-setup package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Minity\ModelSetup;

use yii\db\ActiveQueryInterface;

/**
 * In-One-Place configuration of ActiveRecord {@see setup()}
 */
trait ActiveRecordConfigurationTrait
{
    use ModelConfigurationTrait;

    /**
     * @inheritdoc
     *
     * <code>
     * [
     *      'tableName' => string,
     *      'relations' => [
     *          'rel1' => [
     *              'hasOne'|'hasMany',
     *              modelClassName,
     *              [key => key],
     *              modifierMethodName => [param1, param2, ...],
     *              ...
     *          ],
     *          ...
     *      ],
     * ]
     * </code>
     *
     * @return array
     */
    abstract protected static function setup();

    public static function tableName()
    {
        return static::setup()['tableName'];
    }

    public function __get($name)
    {
        $class = get_class($this);
        if (!isset(self::$config[$class]['relations'])) {
            $config = static::setup();
            self::$config[$class]['relations'] = isset($config['relations']) ? $config['relations'] : [];
        }

        if (isset(self::$config[$class]['relations'][$name])) {
            $query = $this->getRelatedQuery(self::$config[$class]['relations'][$name]);
            $this->populateRelation($name, $query->findFor($name, $this));
        }

        return parent::__get($name);
    }

    public function __call($name, $args)
    {
        if (substr($name, 0, 3) !== 'get') {
            return parent::__call($name, $args);
        }
        $relationName = lcfirst(substr($name, 3));

        $class = get_class($this);
        if (!isset(self::$config[$class]['relations'])) {
            $config = static::setup();
            self::$config[$class]['relations'] = isset($config['relations']) ? $config['relations'] : [];
        }

        return isset(self::$config[$class]['relations'][$relationName])
            ? $this->getRelatedQuery(self::$config[$class]['relations'][$relationName])
            : parent::__call($name, $args);
    }

    private function getRelatedQuery($relation)
    {
        /** @var ActiveQueryInterface $activeQuery */
        $activeQuery = call_user_func([$this, $relation[0]], $relation[1], $relation[2]);
        unset($relation[0], $relation[1], $relation[2]);

        foreach ($relation as $modifierMethod => $args) {
            call_user_func_array([$activeQuery, $modifierMethod], $args);
        }

        return $activeQuery;
    }
}
