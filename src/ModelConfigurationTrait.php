<?php
/*
 * This file is part of the minity/yii2-model-setup package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Minity\ModelSetup;

/**
 * In-One-Place configuration of Model {@see setup()}
 */
trait ModelConfigurationTrait
{
    private static $config = [];

    /**
     * Model configuration.
     *
     * <code>
     * [
     *      'attributes' => [
     *          'attr1' => [
     *              'label' => string,
     *              'hint' => string,
     *              'rules' => [
     *                  string|[validator, param1 => mixed, param2 => mixed, ...],
     *                  ...
     *              ],
     *              'toArray' => bool|string (default false, alias for yii\base\Model::toArray() if string)
     *          ],
     *          ...
     *      ],
     * ]
     * </code>
     *
     * @return array
     */
    abstract protected static function setup();

    public function attributeLabels()
    {
        $class = get_class($this);
        if (!isset(self::$config[$class]['labels'])) {
            $setup = static::setup();
            self::$config[$class]['labels'] = array_filter(
                array_map(function ($config) {
                    return isset($config['label']) ? $config['label'] : null;
                }, isset($setup['attributes']) ? $setup['attributes'] : [])
            );
        }

        return array_merge(parent::attributeLabels(), self::$config[$class]['labels']);
    }

    public function attributeHints()
    {
        $class = get_class($this);
        if (!isset(self::$config[$class]['hints'])) {
            $setup = static::setup();
            self::$config[$class]['hints'] = array_filter(
                array_map(function ($config) {
                    return isset($config['hint']) ? $config['hint'] : null;
                }, isset($setup['attributes']) ? $setup['attributes'] : [])
            );
        }

        return array_merge(parent::attributeHints(), self::$config[$class]['hints']);
    }

    public function rules()
    {
        $class = get_class($this);
        if (!isset(self::$config[$class]['rules'])) {
            $setup = static::setup();
            if (!isset($setup['attributes'])) {
                $rules = [];
            } else {
                $rules = call_user_func_array(
                    'array_merge',
                    array_map(function ($attribute, $config) {
                        return array_map(function ($validator) use ($attribute) {
                            return array_merge([$attribute], (array)$validator);
                        }, isset($config['rules']) ? $config['rules'] : []);
                    }, array_keys($setup['attributes']), $setup['attributes'])
                );
            }

            self::$config[$class]['rules'] = $rules;
        }

        return array_merge(parent::rules(), self::$config[$class]['rules']);
    }

    public function fields()
    {
        $class = get_class($this);
        if (!isset(self::$config[$class]['fields'])) {
            $setup = static::setup();
            if (!isset($setup['attributes'])) {
                $fields = [];
            } else {
                $fields = array_filter(
                    array_combine(
                        array_keys($setup['attributes']),
                        array_map(function ($attribute, $config) {
                            if (!isset($config['toArray']) || false === $config['toArray']) {
                                return null;
                            } elseif (true === $config['toArray']) {
                                return $attribute;
                            } else {
                                return $config['toArray'];
                            }
                        }, array_keys($setup['attributes']), $setup['attributes'])
                    )
                );
            }

            self::$config[$class]['fields'] = $fields;
        }

        return self::$config[$class]['fields'];
    }
}
