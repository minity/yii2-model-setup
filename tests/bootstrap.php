<?php
/*
 * This file is part of the minity/yii2-model-setup package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

require __DIR__ . '/../vendor/autoload.php';
