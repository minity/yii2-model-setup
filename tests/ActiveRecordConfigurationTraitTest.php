<?php
/*
 * This file is part of the minity/yii2-model-setup package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */


class ActiveRecordConfigurationTraitTest extends \PHPUnit\Framework\TestCase
{
    public function testSetup()
    {
        $model = $this->getMockForConfigTrait('\Minity\ModelSetup\ActiveRecordConfigurationTrait', [
            'tableName' => 'some_table',
            'attributes' => [
                'a' => [
                    'label' => 'property A',
                    'rules' => ['required', 'email'],
                    'toArray' => true,
                ],
                'b' => [
                    'label' => 'property B',
                    'hint' => 'hint B',
                    'rules' => [
                        ['in', 'range' => [1, 2, 3]]
                    ],
                    'toArray' => '_b',
                ],
                'c' => [],
            ],
            'relations' => [
                'd' => ['hasOne', $dClass = $this->getMockClass('yii\db\ActiveRecord'), ['id' => 'id']]
            ]
        ]);

        $this->assertEquals(['a' => 'property A', 'b' => 'property B'], $model->attributeLabels());
        $this->assertEquals(['b' => 'hint B'], $model->attributeHints());
        $this->assertEquals([['a', 'required'], ['a', 'email'], ['b', 'in', 'range' => [1, 2, 3]]], $model->rules());
        $this->assertEquals(['a' => 'a', 'b' => '_b'], $model->fields());
        $this->assertEquals('some_table', $model::tableName());

        $query = $this->createMock('\yii\db\ActiveQueryInterface');
        $query->expects($this->once())->method('findFor')->with('d', $model)->willReturn($d = new $dClass);

        $model->method('attributes')->willReturn(['a', 'b', 'c']);
        $model->method('hasOne')->with($dClass, ['id' => 'id'])->willReturn($query);
        $this->assertSame($query, $model->getD());
        $this->assertSame($d, $model->d);
    }

    private function createMockClass($trait, $config)
    {
        $className = uniqid('ActiveRecord');
        $config = var_export($config, true);
        $code = <<<PHP
class {$className} extends \yii\db\ActiveRecord
{
    use {$trait};
    
    public static function setup()
    {
        return {$config};
    }
}
PHP;

        eval($code);

        return $this->getMockClass($className, ['hasOne', 'hasMany', 'attributes']);
    }

    /**
     * @param string $trait
     * @param array $config
     *
     * @return \yii\db\ActiveRecord|\Minity\ModelSetup\ActiveRecordConfigurationTrait|PHPUnit_Framework_MockObject_MockObject
     */
    private function getMockForConfigTrait($trait, array $config)
    {
        $modelClass = $this->createMockClass($trait, $config);

        return new $modelClass;
    }
}
