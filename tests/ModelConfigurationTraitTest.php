<?php
/*
 * This file is part of the minity/yii2-model-setup package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */


class ModelConfigurationTraitTest extends \PHPUnit\Framework\TestCase
{
    public function testSetup()
    {
        $model = $this->getMockForConfigTrait('\Minity\ModelSetup\ModelConfigurationTrait', [
            'attributes' => [
                'a' => [
                    'label' => 'property A',
                    'rules' => ['required', 'email'],
                    'toArray' => true,
                ],
                'b' => [
                    'label' => 'property B',
                    'hint' => 'hint B',
                    'rules' => [
                        ['in', 'range' => [1, 2, 3]]
                    ],
                    'toArray' => '_b',
                ],
                'c' => [],
            ]
        ]);

        $this->assertEquals(['a' => 'property A', 'b' => 'property B'], $model->attributeLabels());
        $this->assertEquals(['b' => 'hint B'], $model->attributeHints());
        $this->assertEquals([['a', 'required'], ['a', 'email'], ['b', 'in', 'range' => [1, 2, 3]]], $model->rules());
        $this->assertEquals(['a' => 'a', 'b' => '_b'], $model->fields());
    }

    private static function createMockClass($trait, $config)
    {
        $className = uniqid('Model');
        $config = var_export($config, true);
        $code = <<<PHP
class {$className} extends \yii\base\Model
{
    use {$trait};
    
    public static function setup()
    {
        return {$config};
    }
}
PHP;

        eval($code);

        return $className;
    }

    /**
     * @param string $trait
     * @param array $config
     *
     * @return \yii\base\Model|\Minity\ModelSetup\ModelConfigurationTrait
     */
    private function getMockForConfigTrait($trait, array $config)
    {
        $modelClass = $this->createMockClass($trait, $config);

        return new $modelClass;
    }
}
